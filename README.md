# Self-touch detector

## Overview
Self-touch is an interesting area in neuroscience.
For research purposes we developed a device for self-touch detection.
Board utilizes Lock-in amplifier technique to reduce noice and for biometric purposes.
Skin impedance is measured at frequency of 50kHz.
Events recognised as self-touches are stored on SD card along side with local timestamp.
Event detection is also signalized with RGB LED.


![Video of function](media/VID_20240319_094648.mp4)

For safety reasons every other communication with computer must be provided over galvanicly insulated USB.

Form impedance measurement op amp in a form of a current source is used to set current through participants body.
For safety reasons device is battery operated and such must not be operated when being charged.

## User interface
To control the device obe push button is used and RGB LED for visual feedback.
Additionally board sends data and diagnostic messages over serial interface (USB micro).
Please make sure you are connected over galvanicly insulated USB.

| Action or state           | Button press      | LED         | Note                                                                                                         |
|---------------------------|-------------------|-------------|--------------------------------------------------------------------------------------------------------------|
| Normal state              | -                 | Light blue  |                                                                                                              |
| Error                     | -                 | Red         | Check Serial for more details                                                                                |
| Card removed              | -                 | Red blink   | Device will not respond until card is inserted                                                               |
| Starting/Stopping capture | -                 | Orange      | If card is not inserted, LED shines orange until is inserted                                                 |
| Self calibration          | One short press   | Blue        | Device will calibrate and returns to normal mode. It is not possible to start capture without a calibration. |
| Lower measurement current | two short presses | Green blink | For more information refer to the current table                                                              |
| Start/stop capture        | Long press        | Green       | Device is logging                                                                                            |

## Setting measuering current
Device can set 7 measuring currents using this part of circuit.

![circuit](media/current_schem.png)

Signals `current_select_x_A` connect resistors `R9-R11` into parallel. 
Trimmer RV1 is currently not connected.
Switches are controlled from MCU and are "active low" (lon on mcu pin sets high on switch input --> closes the switch).

In the table Resistance refers to **ideal case, ignoring the resistance of the switch (~100R-500R)**.
**Currents in the table are only maximum current estimation.**

| Index | Sequence | Resistance | Current [uA] |
|-------|----------|------------|--------------|
| 0     | 0b000    | 760R       | 217          |
| 1     | 0b100    | 820R       | 201          |
| 2     | 0b010    | 910R       | 181          |
| 3     | 0b110    | 1000R      | 115          |
| 4     | 0b001    | 3200R      | 51           |
| 5     | 0b101    | 4700R      | 35           |
| 6     | 0b010    | 10000R     | 16           |

## Electrode placement
For electrode leads we used cable generally shipped with ECG modules such as ![this](https://www.laskakit.cz/ekg-monitoring-srdecni-frekvence-ad8232/). 
We achieved best results with this electrode placement.

![electrode_placement](media/soFarBestElectrodePlacement.png)

Other placements could cause oscilattions of event detection (for example if yellow and green would be flipped).

We used standar ECG sticking electrodes with a dose of **ultrasonograph gel** to reduce the contact resistance.
**This addioanal coating is very important for the function of the device**

## Construction Notes
In this repo we provide full schematics with notes and recommended modules.
For a battery we recommend two 18650s in parallel. 
Also we added case STLs and Fusion360 file in case you need to adjust for you printer.
![BatteryConnection](media/baterry_close.jpg)
![BatteryConnection](media/battery.jpg)

## Detection method
To detect events we first filter the higly oversampled signal using moving average. 
During self-calibration mean value is computed.
From this mean value we derive detection thresholds for comparison with hysteresis (see figure).

![singal figure](media/signalFigure.png)

## Config parameters
To configure a capture, file `config.json` on SD card is used.
Example can be found in `arduino_code/skinImpedance` folder.
| Parameter          | Explenation                                                                                                         | Example value |
|--------------------|---------------------------------------------------------------------------------------------------------------------|---------------|
| name               | String, Name of the participant                                                                                     | "Petr_Novak"  |
| maxLogTime         | Uint, Maximum time in seconds for the capture to log                                                                | 10            |
| clockScalingFactor | Float, Scaling the time constants. Over larger period of time, frequency inaccuracy can cause wrong time intervals  | 0.87          |
| selfCalibPeriod    | Uint, Time over which the device calibrates it self in seconds                                                      | 10            |
| detectionOffsetP   | Uint, Detection threshold from mean value for comparison with hysteresis                                            | 10            |
| detectionOffsetN   | Uint, Detection threshold from mean value for comparison with hysteresis                                            | 5             |
## Storing of values and logging
In current version, data are stored on the SD card at the end of the capture. 
**The buffer for captured data has fixed size of 1024 events.
If this number is exceeded the buffer is reset and logging starts over.**

For each capture a directory is created on a SD card. 
The directory name consists of a time of start of the capture (time from powerup) in MM-SS format.
Inside the directory, are two files, one with experiment information (Name of participant, time of start etc.) and second time with captured events.

Captured events are formated as `timestamp, polarity`

![example of data](media/exampleOfData.png)

## PCB design
Board is designed in KiCad 7.0.

![front view](media/pcb_front.png)

![bottom view](media/pcb_back.png)

