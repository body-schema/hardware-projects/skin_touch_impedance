// An example of an external SPI driver.
//
#include <SPI.h>
#include "hardware/spi.h"
#include "SdFat.h"

// SD chip select pin.
 #define SD_CS_PIN 13
#define SD_MISO_PIN 11
#define SD_MOSI_PIN 12
#define SD_SCK_PIN 10
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI, SD_SCK_MHZ(50), &mySpi)

// This is a simple driver based on the the standard SPI.h library.
// You can write a driver entirely independent of SPI.h.
// It can be optimized for your board or a different SPI port can be used.
// The driver must be derived from SdSpiBaseClass.
// See: SdFat/src/SpiDriver/SdSpiBaseClass.h
class MySpiClass : public SdSpiBaseClass {
 public:
  // Activate SPI hardware with correct speed and mode.
  void activate() {
    spi_init(spi1,1000000);
    gpio_set_dir(SD_CS_PIN, GPIO_OUT);
    gpio_set_function(SD_MISO_PIN, GPIO_FUNC_SPI);
    gpio_set_function(SD_MOSI_PIN, GPIO_FUNC_SPI);
    gpio_set_function(SD_SCK_PIN, GPIO_FUNC_SPI);
    spi_set_baudrate(spi1, 1000000);
    digitalWrite(SD_CS_PIN,LOW);
  }
  // Initialize the SPI bus.
  void begin(SdSpiConfig config) {
    (void)config;
    spi_init(spi1,1000000);
    gpio_set_dir(SD_CS_PIN, GPIO_OUT);
    gpio_set_function(SD_MISO_PIN, GPIO_FUNC_SPI);
    gpio_set_function(SD_MOSI_PIN, GPIO_FUNC_SPI);
    gpio_set_function(SD_SCK_PIN, GPIO_FUNC_SPI);
    spi_set_baudrate(spi1, 1000000);
    digitalWrite(SD_CS_PIN,LOW);
  }
  // Deactivate SPI hardware.
  void deactivate() { spi_deinit(spi1); }
  // Receive a byte.
  uint8_t receive() {
    uint8_t ret;
    spi_read_blocking(spi1,0xff, &ret, 1);
    return ret;
    }
  // Receive multiple bytes.
  // Replace this function if your board has multiple byte receive.
  uint8_t receive(uint8_t* buf, size_t count) {
    spi_read_blocking(spi1,0xff, buf, count);
    return 0;
  }
  // Send a byte.
  void send(uint8_t data) { spi_write_blocking(spi1,&data,1); }
  // Send multiple bytes.
  // Replace this function if your board has multiple byte send.
  void send(const uint8_t* buf, size_t count) {
    spi_write_blocking(spi1,buf,count);
  }
  // Save SPISettings for new max SCK frequency
  void setSckSpeed(uint32_t maxSck) {
    spi_set_baudrate(spi1, maxSck);
  }

 private:
  SPISettings m_spiSettings;
} mySpi;
