#include <hardware/pwm.h>
#include <hardware/pll.h>
#include <hardware/clocks.h>
#include "custom_spi_driver.h"

// SD card includes
#include <SPI.h>
#include "SdFat.h"

#include <ArduinoJson.h>

#include <Int64String.h>

// define colors macros
#define COLOR_CTU_BLUE 2666, 8470, 9215
#define COLOR_RED 10000, 0, 0
#define COLOR_GREEN 0, 10000, 0
#define COLOR_BLUE 0, 0, 10000
#define COLOR_ORANGE 9882, 2700, 3
#define COLOR_BLACK 0, 0, 0

// define ports names
const uint8_t signal_in = 28;
const uint8_t ps = 23;

const uint8_t led_red = 6;
const uint8_t led_green = 8;
const uint8_t led_blue = 7;

const uint8_t button = 9;
bool prevButtonState;
uint64_t startTimeStamp;
uint64_t curTimeStamp;
uint64_t startOfCaptureStamp;
uint64_t captureStamp;
#define LONG_PRESS_TIME 1000000
#define SHORT_PRESS_TIME 500000
#define SHORT_DELAY_TIME 750000
#define BLINK_TIME 100000


const uint8_t signal_out = 17;
const uint8_t signal_out_inv = 16;
const uint8_t current_select[3] = { 18,19,20 };

const uint8_t cspin = 13;
const uint8_t FAKEcspin = 5;
const uint8_t SPI_Rx = 11;
const uint8_t SPI_Tx = 12;
const uint8_t SPI_CLK = 10;
const uint8_t CARD_DETECT = 14;
const uint8_t CARD_ENABLE = 15;
bool CARD_DETECT_prev = false;

// set color
void set_led_color(uint16_t R, uint16_t G, uint16_t B);
void get_led_color(uint16_t *color);
void set_current(uint8_t state);

bool clk_set = false;
uint slice_num;


uint32_t fs = 100000;
uint64_t toWait = (uint64_t)1000000 / fs;
uint64_t cycleTook;
uint64_t loopTook;
#define CAPTURE_BUFFER_SIZE 512
uint8_t oversampling = 9;
#define FILTER_BUFFER_SIZE 512
#define EVENTS_BUFFER_SIZE 1024

uint64_t minDetectPeriod = 1000;  // 1000us = 1ms
uint16_t detectThreshold_P = 4096;
uint16_t detectThreshold_N = 4096;
uint16_t longTermSignalMinimum;
uint16_t detectionOffsetN;
uint16_t detectionOffsetP;
#define EVENT_TOUCH 1
#define EVENT_RELEASE -1


// file management variables
#define CONFIG_FILE_NAME "config.json"
#define MAX_FILE_BYTES 3000
char logFileName[32];
long maxLogTime;
float clkScaleFactor;
char configFile[MAX_FILE_BYTES];
bool cardLost = false;
uint64_t selfCalibPeriod;
uint32_t selfCalibTime;
uint64_t lastShortPressStamp = 0;
uint8_t shortPressCnt = 0;
uint8_t current_index = 0;
uint8_t current_map[7] = {0,4,2,6,1,5,3};
uint16_t current_values[7] = {217,201,181,115,51,35,16};

SdFs SD;
FsFile openedFile;


void readConfig(bool initNeeded = false);
void startStopLog();
void storeData(uint32_t chunk);
void storeRest();

uint16_t read_avg(uint8_t analogPin, uint8_t shift, uint32_t T_us);

// "semaphores" because we do not have any RTOS
bool allSetUp = false;
bool selfCalibrateReq = false;
bool selfCalibrateTermReq = false;
bool run = false;

bool touchEventDetected = false;
bool prevTouchDetected = false;
bool releaseEventDetected = false;
bool blinkRequest = false;
bool blink = false;
bool send_event = 0;
uint16_t prevColor[3];
uint64_t blinkStart;
bool long_press = false;

struct {
  uint16_t *data;
  uint32_t tail;
  uint32_t head;
  uint32_t sum;
  uint16_t filteredValue;
  uint64_t longTermMeanValue;
} dataCaptured;

struct {
  int8_t *data;
  uint64_t *timeStamps;
  uint64_t tail;
  uint64_t head;
} eventsDetected;


void setup() {
  // delay(5000);
  //set LED outputs
  slice_num = pwm_gpio_to_slice_num(led_red);
  pwm_set_clkdiv(slice_num, 120);
  pwm_set_wrap(slice_num, 10000);  // 120e6[Hz]/(120*10000) = 100[Hz]
  pwm_set_enabled(slice_num, true);
  slice_num = pwm_gpio_to_slice_num(led_green);
  pwm_set_clkdiv(slice_num, 120);
  pwm_set_wrap(slice_num, 10000);  // 120e6[Hz]/(120*10000) = 100[Hz]
  pwm_set_enabled(slice_num, true);
  slice_num = pwm_gpio_to_slice_num(led_blue);
  pwm_set_clkdiv(slice_num, 120);
  pwm_set_wrap(slice_num, 10000);  // 120e6[Hz]/(120*10000) = 100[Hz]
  pwm_set_enabled(slice_num, true);
  set_led_color(COLOR_RED);

  // initialize data structures
  dataCaptured.tail = 0;
  dataCaptured.head = 0;
  dataCaptured.data = (uint16_t *)calloc(CAPTURE_BUFFER_SIZE, sizeof(uint16_t));
  dataCaptured.sum = 0;
  
  eventsDetected.tail = 0;
  eventsDetected.head = 0;
  eventsDetected.data = (int8_t *)malloc(EVENTS_BUFFER_SIZE * sizeof(int8_t));
  eventsDetected.timeStamps = (uint64_t *)calloc(EVENTS_BUFFER_SIZE, sizeof(uint64_t));


  // set ADC resolution and lower noise of on board dcdc
  analogReadResolution(12);
  gpio_set_dir(ps, GPIO_OUT);
  digitalWrite(ps, HIGH);

  //set outputs
  Serial.println("Initializing GPIO");
  gpio_set_function(led_red, GPIO_FUNC_PWM);
  gpio_set_function(led_green, GPIO_FUNC_PWM);
  gpio_set_function(led_blue, GPIO_FUNC_PWM);

  gpio_set_function(signal_out, GPIO_FUNC_PWM);
  gpio_set_function(signal_out_inv, GPIO_FUNC_PWM);

  for (uint8_t i = 0; i < 3; i++) gpio_set_dir(current_select[i], GPIO_OUT);
  gpio_set_dir(button, GPIO_IN);
  gpio_set_pulls(button, true, false);
  delay(100); // this delay is needed for the next digital read to read correct value
  if (digitalRead(button) == HIGH) prevButtonState = true;  // get initial state of the button
  else prevButtonState = false;

  gpio_set_dir(CARD_DETECT, GPIO_IN);
  gpio_set_pulls(CARD_DETECT, true, false);
  if (digitalRead(CARD_DETECT) == HIGH) CARD_DETECT_prev = true;  // get initial state of CD

  gpio_set_dir(LED_BUILTIN, GPIO_OUT);
  gpio_set_dir(CARD_ENABLE, GPIO_OUT);
  digitalWrite(CARD_ENABLE, HIGH);

  Serial.println("Initializing driving signals");

  // set driving signals PWM
  slice_num = pwm_gpio_to_slice_num(signal_out_inv);
  pwm_set_clkdiv(slice_num, 15);
  pwm_set_wrap(slice_num, 100);  // 120e6[Hz]/(2400) = 50[kHz]
  pwm_set_gpio_level(signal_out_inv, 50);
  pwm_set_gpio_level(signal_out, 50);  //square wave
  pwm_set_enabled(slice_num, true);

  set_current(0); // set maximum current
  //init SD card
  Serial.println("Initializing SPI");
  readConfig(true);
  delay(10);

  // all set up, change RGB to Blue
  set_led_color(COLOR_CTU_BLUE);
  allSetUp = true;
  Serial.println("Setup 0 done");
}
void setup1() {

  while (!allSetUp) delay(1);
  Serial.println("Setup 1 done");
}

void loop() {
  // with 1kHz refresh rate
  // scan button
  curTimeStamp = time_us_64();
  bool buttonState = digitalRead(button);
  if (!buttonState && prevButtonState) {  //falling edge - button pressed
    startTimeStamp = time_us_64();

  } else if (buttonState && !prevButtonState) {  // rising edge - button released
    if (!run && !selfCalibrateReq && (long)(curTimeStamp - startTimeStamp) <= SHORT_PRESS_TIME && (long)(curTimeStamp - startTimeStamp) < LONG_PRESS_TIME) {
      //if button pressed for long enough run self calibration
      shortPressCnt++;
      lastShortPressStamp = time_us_64();
    }
    if (!selfCalibrateReq && (long)(curTimeStamp - startTimeStamp) >= LONG_PRESS_TIME) {  //if button pressed for long enough
      long_press = true;
    }
  }
  prevButtonState = buttonState;

  // time passed since last short button press
  if (shortPressCnt > 0 && (long)(curTimeStamp - lastShortPressStamp) >= SHORT_DELAY_TIME){
    switch (shortPressCnt) {
      case(1):
        Serial.println("Self calibration");
        selfCalibTime = selfCalibPeriod;
        selfCalibrateReq = true;
        set_led_color(COLOR_BLUE);
        break;
      
      case(2):
        if (++current_index >= 7) current_index = 0; // there some mapping needs to be done 
        set_current(current_index);
        if (!blink) get_led_color(prevColor);
        set_led_color(COLOR_GREEN);
        blinkStart = curTimeStamp;
        blink = true;
        break;              
      
      default:
        break;
      
    }
    shortPressCnt = 0;    
  }  

  if (long_press and detectThreshold_P != 4096){
    startStopLog();
    long_press = false;
  }

  // reset after self calibration
  if (selfCalibrateReq && selfCalibrateTermReq) {
    selfCalibrateReq = false;
    selfCalibrateTermReq = false;
    set_led_color(COLOR_CTU_BLUE);
    Serial.println("self calib finished");
    Serial.print("Lower thr: ");
    Serial.println(detectThreshold_N);
    Serial.print("Upper thr: ");
    Serial.println(detectThreshold_P);
  }

  // reset after timeup
  if (run & (long)(curTimeStamp - startOfCaptureStamp) >= maxLogTime * 1000000) {
    Serial.println("Timeout");
    startStopLog();
  }

  // check if card was inserted
  if (digitalRead(CARD_DETECT) == LOW && CARD_DETECT_prev) {
    Serial.println("card detected");
    CARD_DETECT_prev = false;
    readConfig(true);

  } else if (digitalRead(CARD_DETECT) == HIGH && !CARD_DETECT_prev) {  // check if card was removed
    Serial.println("card removed");
    CARD_DETECT_prev = true;
    if (!blink) get_led_color(prevColor);
    set_led_color(COLOR_RED);
    blinkStart = curTimeStamp;
    blink = true;
  }

  // blink when event
  if (blinkRequest) {
    uint16_t prevTmpColor[3];
    if (!blink) get_led_color(prevColor);
    set_led_color(COLOR_BLACK);
    blinkStart = curTimeStamp;
    blink = true;
    blinkRequest = false;
  } 
  // restore color after blink
  if(blink && (long)(curTimeStamp-blinkStart)>= BLINK_TIME) {
    blink = false;
    set_led_color(prevColor[0], prevColor[1], prevColor[2]);
  }
  // send event information over serial
  if (send_event){
      Serial.print("event ");
      Serial.print(eventsDetected.timeStamps[eventsDetected.head-1]);
      Serial.print(", ");
      Serial.println(eventsDetected.data[eventsDetected.head-1]);
      send_event = false;    
  }


  // store data
  if(run) storeData(EVENTS_BUFFER_SIZE/10);
  loopTook = curTimeStamp - time_us_64();
  if (1000 > loopTook) delayMicroseconds(1000 - loopTook);
}

void loop1() {
  captureStamp = time_us_64();
  
  // Filter using moving average 
  dataCaptured.sum -= dataCaptured.data[dataCaptured.head];
  dataCaptured.data[dataCaptured.head] = analogRead(signal_in);
  dataCaptured.sum += dataCaptured.data[dataCaptured.head];
  dataCaptured.filteredValue = dataCaptured.sum>>oversampling;

  // detect positive spike
  if (detectThreshold_P != 4096){ // if at least one self calibration 
    if (dataCaptured.filteredValue >= detectThreshold_P && !prevTouchDetected) {  // If current value is lower than set treshold
      touchEventDetected = true;
      prevTouchDetected = true;
    }
    // detect negative spike
    if (dataCaptured.filteredValue <= detectThreshold_N && prevTouchDetected) {  // if spike is bigger than threshold
      releaseEventDetected = true;
      prevTouchDetected = false;
    }
  }
  

  if (touchEventDetected || releaseEventDetected) {
    eventsDetected.timeStamps[eventsDetected.head] = captureStamp;
    eventsDetected.data[eventsDetected.head] = touchEventDetected ? EVENT_TOUCH : EVENT_RELEASE;
    if ((++eventsDetected.head) >= EVENTS_BUFFER_SIZE) {  //if events buffer full
      eventsDetected.head = 0;
    }
    blinkRequest = true;
    send_event = true;
  }

  if (selfCalibrateReq && !selfCalibrateTermReq) {      // get long term everage
    if (selfCalibTime == selfCalibPeriod) {     // in the begining reset values
      dataCaptured.longTermMeanValue = 0;
      longTermSignalMinimum = 4096;               
    }
    dataCaptured.longTermMeanValue += dataCaptured.filteredValue;
    
    if (!(--selfCalibTime)) { // after done with sampling
      dataCaptured.longTermMeanValue = dataCaptured.longTermMeanValue / selfCalibPeriod;
      detectThreshold_N = dataCaptured.longTermMeanValue + detectionOffsetN;
      detectThreshold_P = dataCaptured.longTermMeanValue + detectionOffsetP;
      selfCalibrateTermReq = true;
    }
  }

  if ((++dataCaptured.head) >= CAPTURE_BUFFER_SIZE) {  //if capture buffer full
    dataCaptured.head = 0;
  }
  
  touchEventDetected = false;
  releaseEventDetected = false;

  cycleTook = captureStamp - time_us_64();
  if (cycleTook < toWait) delayMicroseconds(toWait - (cycleTook));
}

void set_led_color(uint16_t R, uint16_t G, uint16_t B) {
  pwm_set_gpio_level(led_red, R);
  pwm_set_gpio_level(led_green, G);
  pwm_set_gpio_level(led_blue, B);
}

void set_current(uint8_t index) {
  Serial.print("Current set to ");
  Serial.print(current_values[index]); 
  Serial.println(" uA");
  uint8_t state = current_map[index];
  for (uint8_t i = 0; i < 3; i++) {
    digitalWrite(current_select[i], ((state >> i) & 0x01) ? HIGH : LOW);
  }
}

// Currently unused function
uint16_t read_avg(uint8_t analogPin, uint8_t shift, uint32_t T_us){
  shift = shift > 20? 20 : shift;     // clamp shift to 20
  uint32_t avg = 0;
  uint32_t iterations = 1<<shift;
  for(uint32_t i = 0; i < iterations; i++) {
    avg += analogRead(analogPin);
    delayMicroseconds(T_us);
  }
  return (uint16_t)(avg>>shift);
}

void readConfig(bool initNeeded) {
  while (digitalRead(CARD_DETECT) == HIGH) {
    Serial.println("No card!");  //wait for the card to be inserted
    delay(500);
  }
  Serial.println("Card Detected");
  if (initNeeded) {
    while (!SD.begin(SD_CONFIG)) {    //wait for the card init
      Serial.println("Could not init card");
      delay(100);
    }
  }

  FsFile f = SD.open(CONFIG_FILE_NAME, FILE_READ);
  while (!f) {  // spin until config file is present
    set_led_color(COLOR_RED);
    SD.begin(SD_CONFIG);
    f = SD.open(CONFIG_FILE_NAME, FILE_READ);
    Serial.print("no such file ");
    Serial.println(CONFIG_FILE_NAME);
    delay(100);
  }
  Serial.println("file opened");
  f.read(configFile, MAX_FILE_BYTES);
  f.close();
  DynamicJsonDocument config(MAX_FILE_BYTES);
  deserializeJson(config, configFile);
  strcpy_P(logFileName, config["name"]);
  maxLogTime = config["maxLogTime"];
  clkScaleFactor = config["clockScalingFactor"];
  selfCalibPeriod = config["selfCalibPeriod"];
  selfCalibPeriod *= fs;
  detectionOffsetP = config["detectionOffsetP"];
  detectionOffsetP *=1;
  detectionOffsetN = config["detectionOffsetN"];
  detectionOffsetN *=1;
  Serial.print("Log file name: ");
  Serial.println(logFileName);
  Serial.print("Max log time: ");
  Serial.println(maxLogTime);
  Serial.print("Clock scaling Factor: ");
  Serial.println(clkScaleFactor, 5);
  Serial.print("Self-calibration period: ");
  Serial.println(selfCalibPeriod, 5);
  toWait = (uint64_t)(1000000 * clkScaleFactor) / fs;
}

// starts or stops logging into a file and stores the log onto a card
void startStopLog() {
  if (!run) {
    Serial.println("Starting Capture");
    uint64_t tmp;
    bool cardLost = false;
    set_led_color(COLOR_ORANGE);
    Serial.println("Clearing event buffer");
    eventsDetected.head = 0;
    eventsDetected.tail = 0;
    Serial.println("Checking Card");
    while (digitalRead(CARD_DETECT) == HIGH) cardLost = true;  // wait until card is re-inserted
    Serial.println("Card found");
    if (cardLost) {
      readConfig(true);
    }
    tmp = time_us_64() / 1000000;
    uint8_t minutes = (uint8_t)(tmp) / 60;
    uint8_t seconds = tmp - minutes * 60;
    String directory = "/" + (String)logFileName + (String) "_" + String(minutes) + "-" + String(seconds) + "/";
    SD.mkdir(directory);
    String fileName = directory + "info.txt";
    openedFile = SD.open(fileName, FILE_WRITE);
    char info[100];
    tmp = time_us_64();
    
    sprintf(info, "{\n\t\"name\":\"%s\",\n\t\"", logFileName);
    openedFile.write(info);
    sprintf(info, "timeOfStart\":%ld\n\t\"", tmp);
    openedFile.write(info);
    sprintf(info, "current [uA]\":%u\n}", current_values[current_index]);
    openedFile.write(info);
    
    openedFile.close();
    fileName = directory + "log.txt";
    openedFile = SD.open(fileName, FILE_WRITE);
    set_led_color(COLOR_GREEN);
    startOfCaptureStamp = time_us_64();
    run = true;
  } else {
    run = false;
    Serial.println("Stopping Capture");
    set_led_color(COLOR_ORANGE);
    Serial.println("Storing rest");
    storeRest();
    Serial.println("Storing complete");
    openedFile.close();
    set_led_color(COLOR_CTU_BLUE);
  }
}
void storeRest() {
  storeData(EVENTS_BUFFER_SIZE);
}

void storeData(uint32_t chunk) {
  char ptr[5];
  String stamp;
  String data;
  String store;
  for (uint32_t i = 0; i < chunk; i++) {
    if (eventsDetected.tail == eventsDetected.head) break;
    stamp = int64String(eventsDetected.timeStamps[eventsDetected.tail]);
    data = String(eventsDetected.data[eventsDetected.tail]);
    store = stamp + ", " + data + "\n";
    openedFile.write(store.c_str());
    if ((++eventsDetected.tail) >= EVENTS_BUFFER_SIZE) eventsDetected.tail = 0;
  }
}


void get_led_color(uint16_t *color) {
  uint slice = pwm_gpio_to_slice_num(led_red);
  uint chan = pwm_gpio_to_channel(led_red);
  color[0] = (uint16_t)((pwm_hw->slice[slice].cc) >> (chan ? PWM_CH0_CC_B_LSB : PWM_CH0_CC_A_LSB));


  slice = pwm_gpio_to_slice_num(led_green);
  chan = pwm_gpio_to_channel(led_green);
  color[1] = (uint16_t)((pwm_hw->slice[slice].cc) >> (chan ? PWM_CH0_CC_B_LSB : PWM_CH0_CC_A_LSB));

  slice = pwm_gpio_to_slice_num(led_blue);
  chan = pwm_gpio_to_channel(led_blue);
  color[2] = (uint16_t)(pwm_hw->slice[slice].cc >> (chan ? PWM_CH0_CC_B_LSB : PWM_CH0_CC_A_LSB));
}
